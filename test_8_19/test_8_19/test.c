#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

//int main()
//{
//	char acHello[] = "hello\0world";
//	char acNew[15] = { 0 };
//	strcpy(acNew, acHello);
//	int a = strlen(acNew);
//	int b = strlen(acHello);
//	return 0;
//}

int FBC(int n)
{
	if (n <= 0)
	{
		printf("输入有误");
		return 0;
	}

	if (n == 1)
		return 0;
	if (n == 2)
		return 1;
	int f1 = 0;
	int f2 = 1;
	int i = 3;
	while (i++ <= n)
	{
		f2 = f1 + f2;
		f1 = f2 - f1;
	}
	return f2;
}


int main()
{
	int n = 0;
	scanf("%d", &n);
	printf("第n个数为：%d", FBC(n));
	return 0;
}