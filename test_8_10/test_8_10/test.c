#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

int main()
{
	char ch = 'w';
	char* pc = &ch;

	const char* p = "hello world";  //使用const来约束指针，使其不可更改，指向常量字符串

	printf("%c\n", *p);
	printf("%s\n", p);
	return 0;
}