#pragma once
#define ROW 3
#define COL 3
#include<stdlib.h>
#include<time.h>
void InitBoard(char board[ROW][COL], int row, int col);

void DisplayBoard(char board[ROW][COL], int row, int col);

void Player_Move(char board[ROW][COL], int row, int col);

void Computer_Move(char board[ROW][COL], int row, int col);

char IfWin(char board[ROW][COL], int row, int col);
//判断胜负
//玩家赢返回*
//电脑赢返回#
//平局返回 Q
//继续返回 C