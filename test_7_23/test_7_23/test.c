#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include"add.h"

//int main()
//{
//	int a = 5;
//	int b = 6;
//	printf("%d", add(a, b));
//	return 0;
//}



//void print(unsigned x)
//{
//	if (x > 9)
//	{	
//		print(x / 10);
//	}
//	printf("%d", x % 10);
//}
//
//int main()
//{
//	unsigned num = 456123;
//	print(num);
//	return 0;
//}


int my_strlen(char* s)
{
	if (*s != '\0')
	{
		return 1 + my_strlen(s + 1);
	}
	else
	{
		return 0;
	}
}

int main()
{
	char s[] = "abcde";
	int len = my_strlen(s);
	printf("%d", len);
	return 0;
}