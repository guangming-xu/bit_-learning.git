#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//int main()
//{
//	int ch = 0;
//	while ((ch = getchar()) != EOF)  
//	{
//		putchar(ch);
//	}
//	return 0;
//}

//
//int main()
//{
//	char password[20] = { 0 };
//	printf("请输入密码:>");
//	scanf("%s", password); //此时有个/n，所以应该先清除这个/n
//	
//	getchar();  //清除缓冲区
//
//	printf("请确认密码（Y/N）:>");
//	int ch = getchar();
//	if (ch == 'Y')
//	{
//		printf("确认成功\n");
//	}
//	else
//	{
//		printf("确认失败\n");
//	}
//
//	return 0;
//}


#include<math.h>
int change(int n)
{
    int i = 0;
    while (pow(6, i) <= n)
    {
        i++;
    }
    int a = pow(6, --i);
    int b = n - a;
    int j = 0;
    int num = 1;
    for (j; j < i; j++)
    {
        num *= 10;
    }
    if (b != 0)
    {
        return num + change(b);
    }
    else
    {
        return num;
    }
}

int main()
{
    int n;
    scanf("%d", &n);
    printf("%d", change(n));
    return 0;
}