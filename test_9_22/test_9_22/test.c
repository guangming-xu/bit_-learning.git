#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
//
//int main()
//{
//	//静态分配
//	int n = 10;
//	int arr[10];
//
//	return 0;
//}

//动态分配

//int main()
//{
//	//arr[10] = 0;
//	int* p = (int*)malloc(40);
//	if (p == NULL)
//	{
//		return -1;
//	}
//
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i;
//	}
//
//
//	//释放空间
//	free(p);  //虽然释放了空间，但是p指针还是指向那块地址，因此将p置空
//	p = NULL;  
//	return 0;
//}

int main()
{
	//calloc 申请的空间会初始话为0
	int* p = (int*)calloc(10000000000000, sizeof(int));
	if (p == NULL)
	{
		printf("%s\n", strerror(errno));
		return -1;
	}
	//申请成功
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		printf("%d ", *(p + i));
	}


	//空间不够了，增加空间至20个 int
	int *ptr = realloc(p, 20 * sizeof(int));
	free(p);
	p = NULL;
	return 0;
}