#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
//
////memcmp
//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 1,2,3,6,6 };
//
//	int ret = memcmp(arr1, arr2, 13);
//
//	printf("%d\n",ret);
//	return 0;
//}

//memset

//int main()
//{
//	int arr[] = { 1,2,3,4,5,6 };
//	memset(arr, 0, 24);
//
//	return 0;
//}


//strerror

//int main()
//{
//	printf("%s\n", strerror(0));
//	printf("%s\n", strerror(1));
//	printf("%s\n", strerror(2));
//	printf("%s\n", strerror(3));
//
//	return 0;
//}

//struct Point
//{
//	int x;
//	int y;
//}p3 = { 5,6 };
//
//struct Point p2 = { 1,2 };
//
//struct S
//{
//	double d;
//	struct Point p;
//	char name[20];
//};
//
//int main()
//{
//	struct Point p1 = { 3,4 };
//	struct S s = { 3.14,{5,6},"zhangsan" };
//	printf("%lf\n", s.d);
//	printf("%d %d\n", s.p.x, s.p.y);
//	printf("%s\n", s.name);
//
//	return 0;
//}

struct S1
{
	char c1;
	int a;
	char c2;
};

int main()
{
	printf("%d", sizeof(struct S1));

	return 0;

}