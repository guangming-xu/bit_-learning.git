#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

//int main()
//{
//	int a = 4;
//	++(a++);
//	return 0;
//}

//int f(int a)
//{
//	int b = 0;
//	static int c = 3;
//	a = c++, b++;
//	return (a);
//} 
//int main()
//{
//	int a = 2, i, k;
//	for (i = 0; i < 2; i++) {
//		k = f(a++);
//	} 
//	printf(" % d\n", k);
//	return 0;
//}

//int x = 3;
//void inc()
//{
//	static int x = 1;
//	x *= (x + 1);
//	return;
//} 
//int main()
//{
//	int i;
//	for (i = 1; i < x; i++) {
//		inc();
//	} 
//	return 0;
//}

typedef int Elemtype;
typedef struct LNode
{
	struct LNode *  next;
	Elemtype data;
}LNode, * LinkList;

LinkList Reverse(LinkList L)
{
	LNode* s = (LinkList)malloc(sizeof(LNode));
	s->next = NULL;
	LNode* p = L;
	if (L == NULL)
		return NULL;
	while (L != NULL)
	{
		p->next = s->next;
		s->next = p;
		L = L->next;
		p = L;
	}
	return s;
}