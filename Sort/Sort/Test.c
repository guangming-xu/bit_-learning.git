#include"Sort.h"

void TestInsertSort()
{
	int a[] = { 6,2,5,8,9,10,1,3 };
	PrintArray(a, sizeof(a) / sizeof(int));
	printf("ֱ�Ӳ��������:\n");
	InsertSort(a, sizeof(a) / sizeof(int));
}

void TestShellSort()
{
	int a[] = { 6,2,5,8,9,10,1,3 ,6,2,5,8,9,10,1,3 ,6,2,5,8,9,10,1,3 };
	PrintArray(a, sizeof(a) / sizeof(int));
	printf("ϣ�������:\n");
	ShellSort(a, sizeof(a) / sizeof(int));
}

void TestSelectSort()
{
	int a[] = { 16,20,5,81,9,10,1,3 };
	PrintArray(a, sizeof(a) / sizeof(int));
	printf("ѡ�������:\n");
	SelectSort(a, sizeof(a) / sizeof(int));
}

void TestHeapSort()
{
	int a[] = { 61,22,51,8,9,101,11,3 };
	PrintArray(a, sizeof(a) / sizeof(int));
	printf("�������:\n");
	SelectSort(a, sizeof(a) / sizeof(int));
}

void TestBubbleSort()
{
	int a[] = { 61,22,51,8,9,101,11,3 };
	PrintArray(a, sizeof(a) / sizeof(int));
	printf("ð�������:\n");
	BubbleSort(a, sizeof(a) / sizeof(int));
}

void TestQuickSort()
{
	int a[] = { 61,22,51,8,9,101,11,3 };
	PrintArray(a, sizeof(a) / sizeof(int));
	printf("���������:\n");
	//QuickSort(a, 0, sizeof(a) / sizeof(int) - 1);
	QuickSortNonR(a, 0, sizeof(a) / sizeof(int) - 1);
	PrintArray(a, sizeof(a) / sizeof(int));
}

void TestMergeSort()
{
	int a[] = { 61,22,51,8,9,101,11,3,12 };
	PrintArray(a, sizeof(a) / sizeof(int));
	printf("�鲢�����:\n");
	//QuickSort(a, 0, sizeof(a) / sizeof(int) - 1);
	//MergeSort(a, sizeof(a) / sizeof(int));
	MergeSortNonR(a, sizeof(a) / sizeof(int));
	PrintArray(a, sizeof(a) / sizeof(int));
}

void TestCountSort()
{
	int a[] = { 61,22,61,81,9,101,101,31,2,31 };
	PrintArray(a, sizeof(a) / sizeof(int));
	printf("���������:\n");
	CountSort(a, sizeof(a) / sizeof(int));
	PrintArray(a, sizeof(a) / sizeof(int));
}

int main()
{
	TestInsertSort();
	printf("\n");
	TestShellSort();
	printf("\n");
	TestSelectSort();
	printf("\n");
	TestHeapSort();
	printf("\n");
	TestBubbleSort();
	printf("\n");
	TestQuickSort();
	printf("\n");
	TestMergeSort();
	printf("\n");
	TestCountSort();
	return 0;
}