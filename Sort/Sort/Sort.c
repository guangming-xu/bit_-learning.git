#include"Sort.h"
#include"Stack.h"
void PrintArray(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void Swap(int* a, int* b)
{
	int temp;
	temp = *a;
	*a = *b;
	*b = temp;
}

//插入排序：
void InsertSort(int* a, int n)
{
	assert(a);

	for (int i = 0; i < n - 1; ++i)
	{
		int end = i;
		int x = a[end + 1];
		while (end >= 0)
		{
			if (a[end] > x)
			{
				a[end + 1] = a[end];
				--end;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = x;
	}
	PrintArray(a, n);
}

void ShellSort(int* a, int n)
{
	int  gap = n;
	while (gap > 1)
	{
		gap /= 2;
		for (int i = 0; i < n - gap; ++i)
		{
			int end = i;
			int x = a[end + gap];
			while (end >= 0)
			{
				if (a[end] > x)
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = x;
		}
	}
	PrintArray(a, n);
}

void SelectSort(int* a, int n)
{
	/*int max, tag;
	for (int i = 0; i < n - 1; i++)
	{
		max = a[0];
		tag = 0;
		for (int j = 1 ; j < n - i; j++)
		{
			if (max < a[j])
			{
				max = a[j];
				tag = j;
			}
		}
		int temp = a[n - 1 - i];
		a[n - 1 - i] = max;
		a[tag] = temp;
	}*/

	int begin = 0; 
	int end = n - 1;
	while (begin < end)
	{
		int min = begin;
		int max = end;
		for (int i = begin; i <= end; i++)
		{
			if (a[i] < a[min])
			{
				min = i;
			}
			if (a[i] > a[max])
			{
				max = i;
			}
		}
		Swap(&a[begin], &a[min]);
		if (begin == max)
		{
			max = min;
		}
		Swap(&a[end], &a[max]);
		++begin;
		--end;
	}
	PrintArray(a, n);
}

void AdjustDown(int* a, int n, int parent)
{
	int child = parent * 2;
	if (child >= n)
	{
		return;
	}
	if (child + 1 < n)
	{
		if (a[child] < a[child + 1])
		{
			++child;
		}
	}
	if (a[parent] < a[child])
	{
		Swap(&a[parent], &a[child]);
		AdjustDown(a, n, child);
	}
}


void HeapSort(int* a, int n)
{
	//建大根堆
	int begin = n / 2 - 1 ;
	while (begin >= 0)
	{
		AdjustDown(a, n, begin);
		--begin;
	}
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[n - 1]);
		AdjustDown(a, end, 0);
		--end;
	}
	PrintArray(a, sizeof(a) / sizeof(int));
}


void BubbleSort(int* a, int n)
{
	int tag = 0;
	for (int i = 0; i < n - 1; i++)
	{
		for (int j = 0; j < n - i - 1; j++)
		{
			if (a[j] > a[j + 1])
			{
				Swap(&a[j], &a[j + 1]);
				tag = 1;
			}
		}
		if (tag == 0)
		{
			break;
		}
	}
	PrintArray(a, n);
}


int GetMidIndex(int* a, int left, int right)
{
	int mid = left + ((right - left) >> 1);
	if (a[left] < a[mid])
	{
		if (a[mid] < a[right])
		{
			return mid;
		}
		else if (a[left] > a[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
	else
	{
		if (a[mid] > a[right])
		{
			return mid;
		}
		else if (a[right] < a[left])
		{
			return right;
		}
		else
		{
			return left;
		}
	}
}


int Partition2(int* a, int left, int right)
{
	int key = a[left];
	int povit = left;
	while (left < right)
	{
		while (left < right && a[right] >= key)
		{
			--right;
		}
		a[povit] = a[right];
		povit = right;

		while (left < right && a[left] <= key)
		{
			++left;
		}
		a[povit] = a[left];
		povit = left;
	}
	a[povit] = key;
	return povit;
}

//hoare版本
int Partition(int* a, int left, int right)
{
	int mini = GetMidIndex(a, left, right);
	Swap(&a[mini], &a[left]);
	int keyi = left;
	while (left < right)
	{
		while (left < right && a[right] > a[keyi])
		{
			--right;
		}

		while (left < right && a[left] < a[keyi])
		{
			++left;
		}
		Swap(&a[left], &a[right]);
	}
	Swap(&a[left], &a[keyi]);
	return left;
}


//前后指针
int Partition3(int* a, int left, int right)
{
	int cur = left + 1;
	int pre = left;
	int key = left;
	while (cur <= right)
	{
		if (a[cur] < a[key])
		{
			Swap(&a[cur], &a[++pre]);
		}
		++cur;
	}
	Swap(&a[key], &a[pre]);
	return pre;
}

void QuickSort(int* a, int left, int right)
{
	if (left >= right)
		return;
	
	//int keyi = Partition(a, left, right);
	//int keyi = Partition2(a, left, right);
	int keyi = Partition3(a, left, right);
	QuickSort(a, left, keyi - 1);
	QuickSort(a, keyi + 1, right);
}

void QuickSortNonR(int* a, int left, int right)
{
	Stack st;
	StackInit(&st);
	StackPush(&st, left);
	StackPush(&st, right);
	while (!StackEmpty(&st))
	{
		int end = StackTop(&st);
		StackPop(&st);

		int begin = StackTop(&st);
		StackPop(&st);

		int keyi = Partition3(a,begin, end);
		if (keyi + 1 < end)
		{
			StackPush(&st, keyi + 1);
			StackPush(&st, end);
		}
		
		if (keyi - 1 > begin)
		{
			StackPush(&st, begin);
			StackPush(&st, keyi - 1);
		}
	}
	StackDestory(&st);
}

void _Mergesort(int* a, int left, int right, int* tmp)
{
	if (left >= right)
	{
		return;
	}
	int mid = (left + right) / 2;
	_Mergesort(a, left, mid, tmp);
	_Mergesort(a, mid + 1, right, tmp);

	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (a[begin1] < a[begin2])
		{
			tmp[i++] = a[begin1++];
		}
		else
		{
			tmp[i++] = a[begin2++];
		}
	}

	while (begin1 <= end1)
	{
		tmp[i++] = a[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = a[begin2++];
	}

	for (int j = left; j <= right; j++)
	{
		a[j] = tmp[j];
	}
}

void MergeSort(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		printf("malloc failed\n");
		exit(-1);
	}
	_Mergesort(a, 0, n - 1, tmp);
	free(tmp);
	tmp = NULL;
}

void MergeSortNonR(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		printf("malloc failed\n");
		exit(-1);
	}
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{
			int begin1 = i;
			int end1 = i + gap - 1;
			int begin2 = i+gap, end2 = i + 2*gap - 1;

			//修正边界
			/*if (end1 >= n)
			{
				end1 = n - 1;
				begin2 = n;
				end2 = begin2 - 1;
			}
			else
			{
				if (begin2 >= n)
				{
					end2 = begin2 - 1;
				}
				else if (end2 >= n)
				{
					end2 = n - 1;
				}
			}*/

			if (end1 >= n || begin2 >= n)
			{
				break;
			}
			if (end2 >= n)
			{
				end2 = n - 1;
			}
			int index = i;
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (a[begin1] < a[begin2])
				{
					tmp[index++] = a[begin1++];
				}
				else
				{
					tmp[index++] = a[begin2++];
				}
			}
			while (begin1 <= end1)
			{
				tmp[index++] = a[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[index++] = a[begin2++];
			}

			for (int j = i; j <= end2; j++)
			{
				a[j] = tmp[j];
			}
		}
		/*for (int i = 0; i < n; i++)
		{
			a[i] = tmp[i];
		}*/
		gap *= 2;
	}
}


void CountSort(int* a, int n)
{
	int max = a[0];
	int min = a[0];
	for (int i = 1; i < n; i++)
	{
		if (a[i] > max)
		{
			max = a[i];
		}
		if (a[i] < min)
		{
			min = a[i];
		}
	}

	int range = max - min + 1;
	int* count = (int*)malloc(sizeof(int) * range);
	if (count == NULL)
	{
		printf("malloc failed\n");
		exit(-1);
	}
	for (int i = 0; i < range; i++)
	{
		count[i] = 0;
	}
	for (int i = 0; i < n; i++)
	{
		count[a[i] - min]++;
	}
	int j = 0;
	for (int i = 0; i < range; ++i)
	{
		while (count[i]--)
		{
			a[j++] = min + i;
		}
	}
}