#include"Stack.h"

void StackInit(Stack* ps)
{
	assert(ps);
	//ps = (Stack*)malloc(sizeof(Stack));
	ps->a = NULL;
	//top初始为0，top指针指向的是栈顶的下一个位置
	ps->top = 0;
	ps->capacity = 0;
}

void StackDestory(Stack* ps)
{
	assert(ps);
	free(ps->a);
	ps->a = NULL;
	ps->top = 0;
	ps->capacity = 0;
}

void StackPush(Stack* ps, DataType x)
{
	assert(ps);
	if (ps->top == ps->capacity)
	{
		int newCapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		DataType* tmp = realloc(ps->a, sizeof(DataType) * newCapacity);
		if (tmp == NULL)
		{
			printf("relloc fail\n");
			exit(-1);
		}
		ps->a = tmp;
		ps->capacity = newCapacity;
	}

	ps->a[ps->top] = x;
	ps->top++;
}

void StackPop(Stack* ps)
{
	assert(ps);
	assert(!StackEmpty(ps));
	ps->top--;
}


DataType StackTop(Stack* ps)
{
	assert(ps);
	assert(!StackEmpty(ps));
	
	return ps->a[ps->top - 1];
}

int StackSize(Stack* ps)
{
	assert(ps);
	return ps->top;
}


bool StackEmpty(Stack* ps)
{
	assert(ps);
	return ps->top == 0;
}