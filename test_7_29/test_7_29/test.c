#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//int main()
//{
//	int a = 10;
//	int* p = &a;
//
//	return 0;
//}

//sizeof  是操作符，不是函数

void test1(int arr[10])
{
	printf("%d", sizeof(arr));
}


void test2(char ch[10])
{
	printf("%d", sizeof(ch));
}

int main()
{
	int arr[10] = { 0 };
	char ch[10] = "abc";
	printf("%d\n", sizeof(arr));
	printf("%d\n", strlen(arr));
	test1(arr);
	test2(ch);
	return 0;
}