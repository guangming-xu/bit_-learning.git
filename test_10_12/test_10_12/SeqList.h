#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
//#define N 1000
//typedef int SLDataType;
//
////静态顺序表
//typedef struct SeqList
//{
//	SLDataType a[N];
//	int size; // 表示数组中存储的有效数据
//}SL;
//
////接口函数
//void SeqListInit(SL* ps);
//void SeqListPushBack(SL* ps, SLDataType x);
//void SeqListPopBack(SL* ps);
//void SeqListPushFront(SL* ps, SLDataType x);
//void SeqListPopFront(SL* ps);

#pragma once
#define N 1000
typedef int SLDataType;

//动态顺序表
typedef struct SeqList
{
	SLDataType * a;
	int size; // 表示数组中存储的有效数据
	int capacity; // 数组实际能存储的空间容量是多大
}SL;


//接口函数
void PrintSeqList(SL* ps);
void SeqListInit(SL * ps);
void SeqListDestory(SL* ps); 
void SeqListCheckCapacity(SL* ps);
void SeqListPushBack(SL* ps, SLDataType x);
void SeqListPopBack(SL* ps);
void SeqListPushFront(SL* ps, SLDataType x);
void SeqListPopFront(SL* ps);