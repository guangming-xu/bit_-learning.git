#include"SeqList.h"

void SeqListInit(SL *ps)
{
	ps->a = NULL;
	ps->size = 0;
	ps->capacity = 0;
}


void SeqListDestory(SL* ps)
{
	free(ps->a);
	ps->a = NULL;
	ps->capacity = ps->size = 0;
}

void SeqListCheckCapacity(SL* ps)
{
	if (ps->size == ps->capacity)
	{
		int newcapcity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		SLDataType* tmp = (SLDataType*)realloc(ps->a, newcapcity * sizeof(SLDataType));
		if (tmp == NULL)
		{
			printf("realloc fail\n");
			exit(-1);
		}

		ps->a = tmp;
		ps->capacity = newcapcity;

	}
}


void SeqListPushBack(SL* ps, SLDataType x)
{
	
	SeqListCheckCapacity(ps);
	ps->a[ps->size] = x;
	ps->size++;
}


//
void SeqListPopBack(SL* ps)
{
	/*if (ps->size > 0)
	{
		ps->size--;
	}*/
	assert(ps->size > 0);
	ps->size--;
}


void SeqListPushFront(SL* ps, SLDataType x)
{
	SeqListCheckCapacity(ps);
	int end = ps->size - 1;
	while (end >= 0)
	{
		ps->a[end + 1] = ps->a[end];
		end--;
	}
	ps->a[0] = x;
	ps->size++;
}

void SeqListPopFront(SL* ps);