#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<assert.h>
//void left_turn_K(char* str, int k)
//{
//	int len = strlen(str);
//	int left = 0;
//	int right = len - 1;
//	int temp;
//	while (left < right)
//	{
//		temp = *(str + left);
//		*(str + left) = *(str + right);
//		*(str + right) = temp;
//		left++;
//		right--;
//	}
//	left = 0;
//	right = len - k - 1;
//	while (left < right)
//	{
//		temp = *(str + left);
//		*(str + left) = *(str + right);
//		*(str + right) = temp;
//		left++;
//		right--;
//	}
//	left = len - k;
//	right = len - 1;
//	while (left < right)
//	{
//		temp = *(str + left);
//		*(str + left) = *(str + right);
//		*(str + right) = temp;
//		left++;
//		right--;
//	}
//}
//
//int main()
//{
//	char s[] = "ABCD";
//	left_turn_K(s, 2);
//	printf("%s", s);
//	return 0;
//}


int judge(char* str1, char* str2)
{
	assert(str1 && str2);  //判断输入的字符串是否为空
	int len = strlen(str1);
	int i = 0;
	int j = 0;
	for (i = 0; i < len; i++)
	{
		j = i;
		int k = 0;
		while ((k < len) && *(str1 + k) == *(str2 + (j % len)))  //找到
		{

			k++;
			j++;
		}
		if (k == len)
			return 1;
	}
	return 0;
}


int main()
{
	char s1[] = "AABCD";
	int len = strlen(s1);
	char* s2 = malloc(sizeof(char) * (len + 1));
	gets(s2);
	int ret = judge(s1, s2);
	if (ret == 1)
	{
		printf("%s是%s旋转后的字符串", s2, s1);
	}
	else
	{
		printf("%s不是%s旋转后的字符串", s2, s1);
	}
	return 0;
}