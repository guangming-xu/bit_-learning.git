#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>

//dest 指向目标空间
//src  指向源字符串

//1
//void my_strcpy(char* dest, char* src)
//{
//	while (*src != '\0')
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	*dest = *src;
//}


//2
//void my_strcpy(char* dest, char* src)
//{
//	while (*src != '\0')
//	{
//		*dest++ = *src++;
//	}
//	*dest = *src;
//}

//3
//void my_strcpy(char* dest, char* src)
//{
//	while (*dest++ = *src++) //当为'\0'时，先拷贝再结束循环
//	{
//		;
//	}
//}


//4
#include<assert.h>

void my_strcpy(char* dest, const char* src)
{
	assert(src != NULL); //断言 - release版本可以优化掉
	while (*dest++ = *src++)
	{
		;
	}
}

int main()
{
	
	char arr1[] = "abcdef";
	char arr2[10] = { 0 };
	my_strcpy(arr2, arr1);
	printf("%s", arr2);

	return 0;
}


//int main()
//{
//	const int num = 10;
//
//	//const修饰指针
//	//const 放在*的左边 ,修饰的是*p  
//	//const int* p = &num;    
//	//*p = 20;
//	//p = &num;
//
//
//	//const 放在*的右边，修饰的是p
//	int* const p = &num;
//	*p = 20;
//	p = &num;
//
//	printf("%d\n", num);
//
//	return 0;
//}

//#include<assert.h>
//int my_strlen(const char* s)
//{
//	int count = 0;
//	assert(s);
//	while (*s++ != '\0')
//	{
//		count++;
//	}
//	return count;
//}
//
//int main()
//{
//	char s[] = "abcdef";
//	printf("%d", my_strlen(s));
//
//	return 0;
//}