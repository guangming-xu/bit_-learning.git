#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main() {
	int num1 = 0;
	int num2 = 0;
	scanf("%d%d", &num1, &num2);
	printf("%d\n", num1 + num2);
	return 0;
}

/*
int main() {
	int num1 = 0;
	int num2 = 0;
	scanf("%d%d", &num1, &num2);
	printf("%d\n", num1 + num2);
	return 0;
}
*/

/*
int main() {
	printf("Hello World");
	return 0;
}
*/


//main函数在一个项目中只能有一个

//main函数还有如下的写成方式
/*
	void main()
	{

	}
	这种形式已经被淘汰


	int main(void){
	}


	
*/