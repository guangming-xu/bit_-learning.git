#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//int main() {
//	 //int a = 0;  //这是变量a
//	//100； //常量
//	//const int a = 10;// 通过const 将a定义为常属性，即不能被改变
//	//a = 100; 这时候改变a的值会发生报错
//
//	const int a = 10;  //具有常属性不能被修改
//	int array1[10] = { 0 };
//	//int array2[a] = { 0 };  定义数组大小时只能用常熟，不能用变量或者const修饰的变量
//	return 0;
//}




//#define 定义的标识符常量

#define MAX 100
int main() 
{
	int a = MAX;
	printf("a = %d\n", a);
	//MAX = 200;  不可修改
}