#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

enum Sex  //枚举类型 默认从0开始编号
{
	MALE,
	FEMALE,
	SECRET

};

enum Sex1  //可以赋值，对于没赋值的，取上一个加1
{
	male = 3,
	female = 7,
	secret
};


int main()
{
	enum Sex s = MALE;
	printf("%d,%d,%d\n", MALE, FEMALE, SECRET+2);  
	printf("%d,%d,%d", male, female, secret);
	return 0;
}