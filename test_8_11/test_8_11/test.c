#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

//void test(int arr[])
//{
//
//}

//void test(int arr[10])
//{
//
//}

//void test(int arr[100]) //语法正确但是不建议
//{
//
//}

//void test(int* p)
//{
//
//}

//void test1(int* arr1[])
//{
//}

//void test1(int* arr1[10])
//{
//}

//void test1(int** p)
//{
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//	test(arr);
//
//	int* arr1[10] = { 0 };
//	test1(arr1);
//
//	return 0;
//}


//void test(int arr[3][5])  //可以全部传
//{
//
//}

//void test(int arr[][5]) //可以省略行，不能省略列
//{
//
//}

//void test(int(*p)[5])  //二维数组的首元素地址是第一行的地址，因此传的参数是第一行的数组指针
//{					   //数组指针就是 int (*p)[5]
//
//}
//
//main()
//{
//	int arr[3][5];
//	test(arr);
//
//	return 0;
//}


//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	/*printf("%p\n", Add);
//	printf("%p\n", &Add);*/
//	//函数名 = &函数名 = 函数的地址
//	
//	int arr[10] = { 0 };
//	//数组指针类型 int(*)[10]
//	int(*p)[10] = &arr;
//	//函数指针类型 int(*)(int,int)
//	int (*pf)(int,int) = &Add; //pf用来存放函数的地址 - pf就是函数指针变量
//
//	int ret = Add(2, 3);
//	printf("%d\n", ret);
//
//	//ret = (*pf)(4, 5);
//	ret = pf(4, 5);
//	printf("%d\n", ret);
//
//	scanf("%d", p[0]);
//	printf("%d", (*p)[0]);
//
//	return 0;
//}

int Add(int x, int y) // int (*)(int,int)
{
	return x + y;
}

int Sub(int x, int y) // int (*)(int,int)
{
	return x - y;
}

int Mul(int x, int y) // int (*)(int,int)
{
	return x * y;
}

int Div(int x, int y) // int (*)(int,int)
{
	return x / y;
}

//
//int main()
//{
//	//int* arr[10]; //整形指针数组
//
//	//int(*pf1)(int, int) = &Add;
//	//int(*pf2)(int, int) = &Sub;
//	//int(*pf3)(int, int) = &Mul;
//	//int(*pf4)(int, int) = &Div;
//
//	//pfArr 就是函数指针的数组
//	int (*pfArr[4])(int, int) = {Add,Sub,Mul,Div}; //函数名 = &函数名 都是函数的地址
//
//	return 0;
//}

void menu()
{
	printf("***************************\n");
	printf("*****1. Add    2. Sub******\n");
	printf("*****3. Mul    4. Div******\n");
	printf("*****     0. Exit   *******\n");
	printf("***************************\n");
}

//int main()
//{
//	int input = 0;
//	do
//	{
//		int x = 0;
//		int y = 0;
//		int ret = 0;
//		menu();
//		printf("请选择:>");
//		scanf("%d", &input);
//		if (input)
//		{
//			printf("请输入两个操作数：");
//			scanf("%d%d", &x, &y);
//		}
//		switch (input)
//		{
//		case 1:
//			ret = Add(x, y);
//			printf("%d\n", ret);
//			break;
//		case 2:
//			ret = Sub(x, y);
//			printf("%d\n", ret);
//			break;
//		case 3:
//			ret = Mul(x, y);
//			printf("%d\n", ret);
//			break;
//		case 4:
//			ret = Div(x, y);
//			printf("%d\n", ret);
//			break;
//		case 0:
//			printf("退出计算器\n");
//			break;
//		default:
//			printf("输入有误");
//		}
//		
//	} while (input);
//
//	return 0;
//}



//int main()
//{
//	int input = 0;
//	do
//	{
//		int x = 0;
//		int y = 0;
//		int ret = 0;
//		menu();
//		printf("请选择:>");
//		scanf("%d", &input);
//		int (*pfArr[5])(int, int) = { 0,Add,Sub,Mul,Div };
//		if (input == 0)
//		{
//			printf("退出计算器\n");
//		}
//		else if (input >= 1 && input <= 4)
//		{
//			printf("请输入两个操作数：\n");
//			scanf("%d %d", &x, &y);
//			ret = pfArr[input](x, y);
//			printf("%d\n", ret);
//		}
//		else
//		{
//			printf("输入有误\n");
//		}
//	} while (input);
//	return 0;
//}

//
//void Calc(int(*pf)(int, int))
//{
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	printf("请输入两个操作数：\n");
//	scanf("%d %d", &x, &y);
//	ret = pf(x, y);
//	printf("%d\n", ret);
//}
//
//int main()
//{
//	int input = 0;
//	do
//	{
//		int x = 0;
//		int y = 0;
//		int ret = 0;
//		menu();
//		printf("请选择:>");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			Calc(Add);
//			break;
//		case 2:
//			Calc(Sub);
//			break;
//		case 3:
//			Calc(Mul);
//			break;
//		case 4:
//			Calc(Div);
//			break;
//		case 0:
//			printf("退出计算器\n");
//			break;
//		default:
//			printf("输入有误");
//		}
//		
//	} while (input);
//
//	return 0;
//}


int main()
{
	int (*pf)(int, int) = &Add;
	int (*pfArr[5])(int, int);

	int(* (*ppfArr[5]))(int, int) = &pfArr;

	return 0;

}