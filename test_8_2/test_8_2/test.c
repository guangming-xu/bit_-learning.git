#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//int my_strlen(char* s)
//{
//	int count = 0;
//	char* start = s;
//	while (*s != '\0')
//	{
//		s++;
//	}
//	return s - start;
//}
//
//int main()
//{
//	char s[] = "abcdef";
//	printf("%d", my_strlen(s));
//	return 0;
//}


int main()
{
	int arr[10];
	//int* p = &arr[5];
	//printf("%d\n", p);
	//printf("%p\n", p);

	printf("%p\n", arr);
	printf("%p\n", &arr[0]);
	printf("%p\n", &arr);

	printf("%p\n", arr+1);
	printf("%p\n", &arr[1]);
	printf("%p\n", &arr + 1);



	//printf("%d\n", sizeof(&arr));
	//printf("%d\n", sizeof(arr));
	return 0;
}

//int main()
//{
//	int a = 10;
//	int* p = &a;
//	int** pp = &p; // pp是二级指针
//
//	printf("%p\n", pp);
//	printf("%p\n", *pp);
//	printf("%p\n", p);
//	**pp = 20;
//	printf("%d", a);
//}


//int main()
//{
//	int arr[10]; //整型数组
//	char ch[5]; //字符数组
//	//指针数组——存放指针的数组
//	// int* 整型指针数组
//	// char* 字符指针数组
//
//	int* parr[5];
//	char* pc[6];
//
//	return 0;
//
//}

//int main()
//{
//	int a = 10;
//	int b = 20;
//	int c = 30;
//
//	int* arr[3] = { &a, &b, &c };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		printf("%d ", *(arr[i]));
//	}
//	return 0;
//}





//struct Point
//{
//	int x;
//	int y;
//};
//
//
////结构体类型
//struct Book
//{
//	char name[20];
//	char author[15];
//	float price;
//}b1, b2; //创建的对象b1,b2是全局变量 - 静态区
//
//
//typedef struct Stu
//{
//	char name[20];
//	int age;
//	char id[20];
//}Stu; //Stu是个类型的别名  此时可以用Stu 代替 struct Stu 来定义对象
//
//int main()
//{
//	struct Book b; //局部变量 -栈区
//
//
//	struct Stu s1 = {"张三",20, "20200000"};
//	Stu s2;
//	struct Stu* pt = &s1;
//
//	printf("%s %d %s", pt->name, pt->age, pt->id);
//	return 0;
//}

//struct S
//{
//	int arr[100];
//	int num;
//	char ch;
//	double d;
//};
//
//void printl(struct S ss)
//{
//	printf("%d %d %d %d %c %lf", ss.arr[0], ss.arr[1], ss.arr[2], ss.num, ss.ch, ss.d);
//}
//
//void print2(struct S* ps)
//{
//	printf("%d %d %d %d %c %lf", ps->arr[0], ps->arr[1], ps->arr[2], ps->num, ps->ch, ps->d);
//}
//
//
//int main()
//{
//	struct S s = { {1,2,3,4,5},100,'w',3.14 };
//	printl(s); //传过去完整的s大小
//	printf("\n");
//	print2(&s); //只传了一个指针大小
//	return 0;
//}