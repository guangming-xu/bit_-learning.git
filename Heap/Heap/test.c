#include"Heap.h"
#
void Test1()
{
	Heap hp;
	HeapInit(&hp);
	HeapPush(&hp, 5);
	HeapPush(&hp, 4);
	HeapPush(&hp, 3);
	HeapPush(&hp, 2);
	HeapPush(&hp, 1);
	HeapPrint(&hp);
	printf("\n");
	HeapPop(&hp);
	HeapPrint(&hp);
}

void PrintTopK(int* a, int n, int k)
{
	Heap hp;
	HeapInit(&hp);

	for (int i = 0; i < k; i++) //����С����
	{
		HeapPush(&hp, a[i]);
	}
	
	for (int i = k; i < n; i++)
	{
		if (a[i] > HeapTop(&hp))
		{
			hp.a[0] = a[i];
			AdjustDown(hp.a, hp.size, 0);
		}
	}

	HeapPrint(&hp);
	HeapDestory(&hp);
}

void TestTopK()
{
	int n = 1000000;
	int* a = (int*)malloc(sizeof(int) * n);
	srand(time(0));
	for (size_t i = 0; i < n; i++)
	{
		a[i] = rand() % 1000000;
	}
	a[5] = 1000000 + 1;
	a[45612] = 1000000 + 2;
	a[552] = 1000000 + 3;
	a[452] = 1000000 + 4;
	a[53] = 1000000 + 5;
	a[4561] = 1000000 + 6; 
	a[612] = 1000000 + 7; 
	a[4230] = 1000000 + 8;
	a[56666] = 1000000 + 9;
	a[532221] = 1000000 + 10;

	PrintTopK(a, n, 10);
}


int main()
{
	//Test1();
	TestTopK();
	return 0;
}