#include"Heap.h"

void HeapInit(Heap* hp)
{
	hp->a = NULL;
	hp->size = hp->capacity = 0;
}


void HeapDestory(Heap* hp)
{
	assert(hp);
	free(hp->a);
	hp->a = NULL;
	hp->size = hp->capacity = 0;
}

void HeapPush(Heap* hp, HPDataType x)
{
	assert(hp);
	if (hp->capacity == hp->size)
	{
		size_t newcapacity = hp->capacity == 0 ?  4 : hp->capacity * 2;
		HPDataType* newtmp = realloc(hp->a, newcapacity * sizeof(HPDataType));
		if (newtmp == NULL)
		{
			printf("realloc fail\n");
			exit(-1);
		}
		hp->a = newtmp;
		hp->capacity = newcapacity;
	}
	hp->a[hp->size++] = x;
	AdjustUp(hp->a, hp->size-1);
}

void HeapPrint(Heap* hp)
{
	for (int i = 0; i < hp->size; i++)
	{
		printf("%d ", hp->a[i]);
	}
}

void HeapPop(Heap* hp)
{
	assert(hp);
	assert(!HeapEmpty(hp));

	Swap(hp->a, &hp->a[hp->size - 1]);
	hp->size--;
	AdjustDown(hp->a, hp->size, 0);
}

HPDataType HeapTop(Heap* hp)
{
	assert(hp);
	assert(!HeapEmpty(hp));
	return hp->a[0];
}

bool HeapEmpty(Heap* hp)
{
	assert(hp);
	return hp->size == 0;
}


int HeapSize(Heap* hp)
{
	assert(hp);
	return hp->size;
}

void Swap(HPDataType* a, HPDataType* b)
{
	HPDataType tmp = *a;
	*a = *b;
	*b = tmp;
}

void AdjustUp(int* a, int child)
{
	assert(a);

	int parent = (child - 1) / 2;

	while (child> 0)
	{
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}


void AdjustDown(int* a, int n, int parent)
{

	//С����
	int child = parent * 2 + 1;
	while (child < n)
	{
		if (child + 1 < n && a[child + 1] < a[child])
		{
			child++;
		}

		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}