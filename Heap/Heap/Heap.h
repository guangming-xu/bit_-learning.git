#pragma once
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<stdbool.h>

typedef int HPDataType;

typedef struct Heap
{
	int capacity;
	int size;
	HPDataType* a;
}Heap;

void HeapInit(Heap* hp);
void HeapDestory(Heap* hp);
void HeapPrint(Heap* hp);
void HeapPush(Heap* hp, HPDataType x);
void HeapPop(Heap* hp);
void Swap(HPDataType* a, HPDataType* b);
void AdjustUp(int* a, int child);
void AdjustDown(int* a, int n, int parent);
bool HeapEmpty(Heap* hp);
int HeapSize(Heap* hp);
HPDataType HeapTop(Heap* hp);

