#pragma once
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#define LNodeDataType int
typedef struct ListNode
{
	LNodeDataType data;
	struct ListNode* next;
	struct ListNode* prev;

}LNode;

//��ʼ��
LNode* InitList();
void PrintList(LNode* phead);
void ListPushBack(LNode* phead, LNodeDataType x);
void ListPopBack(LNode* phead);
void ListPushFront(LNode* phead, LNodeDataType x);
void ListPopFront(LNode* phead);
LNode* ListFind(LNode* phead, LNodeDataType x);
void ListInsert(LNode* phead,LNode* pos, LNodeDataType x);
void ListDelete(LNode* phead, LNode* pos);
void ListErase(LNode* phead);


