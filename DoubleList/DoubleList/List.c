#include"List.h"

//带头结点的循环双链表，初始化时，生成头结点并且首尾相连
LNode* InitList()
{
	LNode* phead = (LNode* )malloc(sizeof(LNode));
	phead->next = phead;
	phead->prev = phead;
	return phead;
}	

//打印链表
void PrintList(LNode* phead)
{
	assert(phead);
	LNode* cur = phead->next;
	while (cur != phead)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("\n");
}

//
void ListPushBack(LNode* phead, LNodeDataType x)
{
	assert(phead);
	LNode* newnode = (LNode*)malloc(sizeof(LNode));
	newnode->data = x;
	LNode* tail = phead->prev;
	tail->next = newnode;
	newnode->prev = tail;
	newnode->next = phead;
	phead->prev = newnode;
}

void ListPopBack(LNode* phead)
{
	assert(phead);
	assert(phead->next != phead);

	LNode* tail = phead->prev;
	LNode* tailprev = tail->prev;

	phead->prev = tailprev;
	tailprev->next = phead;
	free(tail);
	tail = NULL;
}


void ListPushFront(LNode* phead, LNodeDataType x)
{
	assert(phead);
	LNode* newnode = (LNode*)malloc(sizeof(LNode));
	newnode->data = x;

	LNode* headnext = phead->next;
	phead->next = newnode;
	newnode->prev = phead;
	newnode->next = headnext;
	headnext->prev = newnode;
}


void ListPopFront(LNode* phead)
{
	assert(phead);
	assert(phead->next != phead);
	LNode* headnext = phead->next;
	LNode* nextNext = headnext->next;
	phead->next = nextNext;
	nextNext->prev = phead;
	free(headnext);
	headnext = NULL;
}


LNode* ListFind(LNode* phead, LNodeDataType x)
{
	assert(phead);
	assert(phead->next != phead);
	LNode* cur = phead->next;
	while (cur != phead)
	{
		if (cur->data == x)
			return cur;
		cur = cur->next;
	}
	return NULL;
}

void ListDelete(LNode* phead, LNode* pos);


void ListErase(LNode* phead);

