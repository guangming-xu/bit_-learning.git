#include"SList.h"

SLTNode* BuyListNode(SLDataType x)
{
	SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode));
	newnode->data = x;
	newnode->next = NULL;

	return newnode;
}

void SListPrint(SLTNode* phead)
{
	SLTNode* cur = phead;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}


void SListPushBack(SLTNode** pphead, SLDataType x)
{
	SLTNode* newnode = BuyListNode(x);
	
	//想要改变pphead，要传pphead的地址 pphead本身指向的内容就是 SLTNode* 
	if (*pphead == NULL)
	{
		*pphead = newnode;
	}
	else
	{
		SLTNode* tail = *pphead;
		while (tail->next != NULL)
		{
			tail = tail->next;
		}
		tail->next = newnode;
	}
}

void SListPushFront(SLTNode** pphead, SLDataType x)
{
	SLTNode* newnode = BuyListNode(x);

	newnode->next = *pphead;
	*pphead = newnode;
}

void SListPopBack(SLTNode** pphead)
{
	/*if (*pphead == NULL)
	{
		printf("错误：对空链表进行删除");
		return;
	}*/

	assert(*pphead != NULL);

	if ((*pphead)->next == NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	else
	{
		SLTNode* prev = NULL;
		SLTNode* tail = *pphead;
		while (tail->next)
		{
			prev = tail;
			tail = tail->next;
		}

		free(tail);
		tail = NULL;
		prev->next = NULL;
	}
}

void SListPopFront(SLTNode** pphead)
{
	assert(*pphead != NULL);
	SLTNode* nextpoint = (*pphead)->next;
	free(*pphead);
	*pphead = nextpoint;
}

SLTNode* SListFind(SLTNode* phead, SLDataType x)
{
	SLTNode* point = phead;
	while (point != NULL)
	{
		if (point->data == x)
			return point;
		point = point->next;
	}
	return NULL;
}

void SListInsertAfter(SLTNode* pos, SLDataType* x)
{
	SLTNode* newnode = BuyListNode(x);
	newnode->next = pos->next;
	pos->next = newnode;
}


void SListInsert(SLTNode** pphead, SLTNode* pos, SLDataType x)
{
	SLTNode* newnode = BuyListNode(x);
	if (*pphead == pos)
	{
		newnode->next = *pphead;
		*pphead = newnode;
	}
	else
	{
		SLTNode* point = *pphead;
		while (point->next != pos)
		{
			point = point->next;
		}
		newnode->next = pos;
		point->next = newnode;
	}
}


void SListErase(SLTNode** pphead, SLTNode* pos)
{
	if (*pphead == pos)
	{
		*pphead = pos->next;
		free(pos);
		pos = NULL;
	}
	else
	{
		SLTNode* point = *pphead;
		while (point->next != pos)
		{
			point = point->next;
		}
		point->next = pos->next;
		free(pos);
		//pos = NULL;
	}
}

//删除pos后面的结点
void SListEraseAfter(SLTNode* pos)
{
	SLTNode* next = pos->next;
	pos->next = next->next;
	free(next);
	//next == NULL;
}


void SListDestory(SLTNode** pphead)
{
	assert(*pphead);
	SLTNode* cur = *pphead;
	while (cur)
	{
		SLTNode* next = cur->next;
		free(cur);
		cur = next;
	}
	*pphead = NULL;
}