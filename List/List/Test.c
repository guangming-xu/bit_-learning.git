#include"SList.h"

void TestSList1()
{
	SLTNode* plist = NULL;
	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPrint(plist);

	SListPushFront(&plist, 1);
	SListPushFront(&plist, 2);
	SListPushFront(&plist, 3);
	SListPushFront(&plist, 4);


	SListPrint(plist);
}

void TestSList2()
{
	SLTNode* plist = NULL;
	
	SListPushFront(&plist, 1);
	SListPushFront(&plist, 2);
	SListPushFront(&plist, 3);
	SListPushFront(&plist, 4);
	SListPrint(plist);

	SListPopFront(&plist);
	SListPopFront(&plist);
	SListPrint(plist);
	/*SListPopBack(&plist);
	SListPopBack(&plist);
	SListPopBack(&plist);
	SListPopBack(&plist);
	SListPopBack(&plist);
	SListPopBack(&plist); 
	SListPopBack(&plist);*/

}

void TestSList3()
{
	SLTNode* plist = NULL;

	SListPushFront(&plist, 1);
	SListPushFront(&plist, 2);
	SListPushFront(&plist, 3);
	SListPushFront(&plist, 4);
	SListPrint(plist);

	SLTNode* p1 = SListFind(plist, 3); 
	SLTNode* p2 = SListFind(plist, 4);
	SListInsertAfter(p1, 30);
	SListInsertAfter(p2, 40);
	SListPrint(plist);
	SListErase(&plist, p2);
	SListEraseAfter(p1);
	//printf("%p", p1);
	SListPrint(plist);
}

int main()
{
	//TestSList1();
	//TestSList2();
	TestSList3();
	return 0;
}
