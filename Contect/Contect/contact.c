#define _CRT_SECURE_NO_WARNINGS
#include"contact.h"
#include<stdio.h>
void InitContact(struct Contact* pc)
{
	pc->sz = 0;
	memset(pc->data, 0, MAX * sizeof(struct PeoInfo));
}

void AddContact(struct Contact* pc)
{
	if (pc->sz == MAX)
	{
		printf("通讯录满了");
	}
	else
	{
		
		printf("请输入名字:>");
		scanf("%s",pc->data[pc->sz].name); //name是数组，不需要取地址

		printf("请输入年龄:>");
		scanf("%d", &(pc->data[pc->sz].age)); //age是变量要取地址

		printf("请输入性别:>");
		scanf("%s", pc->data[pc->sz].sex);

		printf("请输入电话:>");
		scanf("%s", pc->data[pc->sz].tele);

		printf("请输入地址:>");
		scanf("%s", pc->data[pc->sz].addr);

		printf("添加成功\n");
		pc->sz++;
	}
}

void ShowContact(struct Contact* pc)
{
	int i = 0;
	printf("%15s\t%5s\t%8s\t%15s\t%30s\n\n", "name", "age", "sex", "tele", "addr");
	for (i = 0; i < pc->sz; i++)
	{
		//打印每一个数据
		printf("%15s\t%5d\t%8s\t%15s\t%30s",
			pc->data[i].name,
			pc->data[i].age,
			pc->data[i].sex,
			pc->data[i].tele,
			pc->data[i].addr);
		printf("\n");
	}
}


void DelContact(struct Contact* pc)
{
	int n = 0;
	printf("请输入要删除的序号:>");
	scanf("%d", &n);
	if (n >= pc->sz)
	{
		printf("输入有误！");
	}
	else
	{
		int i = 0;
		for (i = n; i < pc->sz - 1; i++)
		{
			pc->data[i] = pc->data[i + 1];
		}
		printf("删除成功！\n");
		pc->sz--;
	}
}

void Modify(struct Contact* pc)
{
	int n = 0;
	printf("请输入要修改的序号:>");
	scanf("%d", &n);
	if (n >= pc->sz || n < 0)
	{
		printf("输入有误！");
	}
	else
	{
		char s[5][20] = { "姓名","年龄","性别","电话","地址" };
		int i = 0;
		for (i = 0; i < 5; i++)
		{
			int tag = 0;
			printf("是否修改%s？是请输入'1'/否请输入'0':>", s[i]);
			scanf("%d", &tag);
			if (tag == 1)
			{
				switch (i)
				{
				case 0:
					printf("请输入新姓名：");
					scanf("%s", pc->data[n].name);
						break;
				case 1:
					printf("请输入新年龄：");
					scanf("%d", &pc->data[n].age);
					break;
				case 2:
					printf("请输入新性别：");
					scanf("%s", pc->data[n].sex);
					break;
				case 3:
					printf("请输入新电话：");
					scanf("%s", pc->data[n].tele);
					break;
				case 4:
					printf("请输入新地址：");
					scanf("%s", pc->data[n].addr);
					break;
				}
				tag = 0;
			}
		}
		printf("修改成功\n");
	}
}



void Search(struct Contact* pc)
{
	char s[NAME_MAX]="";
	printf("请输入查找联系人的姓名：>");
	scanf("%s", s);
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		if (strcmp(s, pc->data[i].name) == 0)
		{
			printf("%15s\t%5d\t%8s\t%15s\t%30s",
				pc->data[i].name,
				pc->data[i].age,
				pc->data[i].sex,
				pc->data[i].tele,
				pc->data[i].addr);
			printf("\n查找成功\n");
			break;
		}
	}
	if (i == pc->sz)
		printf("查找失败\n");
}

void Empty(struct Contact* pc)
{
	pc->sz = 0;
}

void swap(struct PeoInfo* p1, struct PeoInfo* p2)
{
	struct PeoInfo p;
	p = *p1;
	*p1 = *p2;
	*p2 = p;
}

void Sort(struct Contact* pc)
{
	int i = 0;
	for (i = 0; i < pc->sz - 1; i++)
	{
		int j = 0;
		for (j = 0; j < pc->sz - 1 - i; j++)
		{
			if (strcmp(pc->data[j].name, pc->data[j + 1].name) > 0)
				swap(&pc->data[j], &pc->data[j + 1]);
		}
	}
	printf("排序成功！\n");
}