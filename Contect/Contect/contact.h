#define _CRT_SECURE_NO_WARNINGS
#pragma once

#include<stdio.h>
#include<memory.h>
#include<string.h>

#define NAME_MAX 20
#define SEX_MAX 5
#define TELE_MAX 12
#define ADD_MAX 30

#define MAX 1000

struct PeoInfo
{
	char name[NAME_MAX];
	int age;
	char sex[SEX_MAX];
	char tele[TELE_MAX];
	char addr[ADD_MAX];
};


//ͨѶ¼
struct Contact
{
	//ͨѶ¼�Ŀռ��С
	struct PeoInfo data[MAX];
	int sz;//��¼��ǰͨѶ¼��Ч��Ϣ�ĸ���
};

//��ʼ��ͨѶ¼
void InitContact(struct Contact* pc);

//������ϵ��
void AddContact(struct Contact *pc);

//ɾ����ϵ��
void DelContact(struct Contact *pc);

//�޸���ϵ��
void Modify(struct Contact *pc);

//չʾ��ϵ��
void ShowContact(struct Contact *pc);

//������ϵ��
void Search(struct Contact *pc);

//�����ϵ��
void Empty(struct Contact *pc);

//����ϵ����������
void Sort(struct Contact *pc);