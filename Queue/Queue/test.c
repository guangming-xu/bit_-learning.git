#include"Queue.h"

void Test1()
{
	Queue q;
	QueueInit(&q);
	QueuePush(&q, 1);
	QueuePush(&q, 2);
	QueuePush(&q, 3);
	QueuePush(&q, 4);

	int a = QueueFront(&q);
	int b = QueueBack(&q);
	int size = QueueSize(&q);

	QueuePop(&q);
	QueuePop(&q);

	QueueDestory(&q);
}

int main()
{
	Test1();
	return 0;
}