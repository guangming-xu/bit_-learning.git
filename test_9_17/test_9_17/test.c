#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

struct S
{
	int data[1000];
	int num;
};

void print1(struct S tmp)
{
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		printf("%d ", tmp.data[i]);
	}
	printf("\nnum = %d ", tmp.num);
}

void print2(struct S* ps)
{
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		printf("%d ", ps->data[i]);
	}
	printf("\nnum = %d ", ps->num);
}

//int main()
//{
//	struct S s = { {1,2,3,4,5,6,7,8,9,10}, 100 };
//	//print1(s);
//	print2(&s);
//	return 0;
//}

////位段是可以节省空间的
////位段 - 二进制位
////以四字节
//
//struct A
//{
//	int _a : 2; //_a 2个bit位
//	int _b : 5; //_b 5个bit位
//	int _c : 10;
//	int _d : 30;
//};
//
//int main()
//{
//	printf("%d \n", sizeof(struct A));  //结果是8byte
//	return 0;
//}


////枚举
//
//enum Color
//{
//	RED = 5,
//	GREEN,
//	BLUE = 9
//};
//
//int main()
//{
//	printf("%d\n", RED);
//	printf("%d\n", GREEN);
//	printf("%d\n", BLUE);
//	return 0;
//}

//union Un
//{
//	char c;
//	int i;
//};
//
//int main()
//{
//	union Un u = { 0 };
//	//printf("%d ", sizeof(u));
//	printf("%p\n", &u);
//	printf("%p\n", &u.c);
//	printf("%p\n", &u.i);
//	return 0;
//}
//
//int check_sys()
//{
//	union U
//	{
//		char c;
//		int i;
//	}u;
//	u.i = 1;
//	return u.c;
//}
//
//
//
//int main()
//{
//	/*int a = 1;
//	char* p = (char*)&a;
//	if (*p == 1)
//	{
//		printf("小端存储");
//	}
//	else
//	{
//		printf("大端存储");
//	}*/
//
//	/*union U
//	{
//		char c;
//		int i;
//	}u;
//	u.i = 1;*/
//	//if (check_sys() == 1)
//	//{
//	//	printf("小端");
//	//}
//	//else
//	//{
//	//	printf("大端");
//	//}
//	
//	union Un1
//	{
//		char c[5];
//		int i;
//	}C;
//	C.c[0] = ' ';
//	C.c[1] = ' ';
//	C.c[2] = ' ';
//	C.c[3] = ' ';
//	C.c[4] = 'a';
//
//	return 0;
//}

