#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//strcpy

//int main()
//{
//	char arr1[20] = "xxxxxxxxxx";
//	char arr2[] = "hello";
//
//	strcpy(arr1, arr2);
//	char* ret = strcpy(arr1, arr2);  //新建用来接收返回值
//
//	printf("%s ", arr1);
//	printf("%s", ret);
//
//	return 0;
//}


//int main()
//{
//	char arr[] = "Hello bit";//xxxxx bit
//	char* ret = memset(arr, 'x', 5);//单位是字节
//	printf("%s\n", ret);
//
//	return 0;
//}


//int get_max(int x, int y)
//{
//	return (x > y ? x : y);
//}
//
//int main()
//{
//	int a, b;
//	scanf("%d%d", &a, &b);
//	printf("%d", get_max(a, b));
//	return 0;
//}



Swap(int* a, int* b)
{
	int z = *a;
	*a = *b;
	*b = z;
}

int main()
{
	int a = 20;
	int b = 10;
	Swap(&a, &b);
	printf("%d, %d", a, b);
	return 0;
}