#pragma once
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<stdbool.h>

struct BinaryTreeNode;
//ǰ������

typedef struct BinaryTreeNode* QDataType;

typedef struct QueueNode {
	struct QueueNode* next;
	QDataType data;
}QueueNode;

typedef struct Queue {
	QueueNode* front;
	QueueNode* tail;
}Queue;

void QueueInit(Queue* pq);
void QueueDestory(Queue* pq);
void QueuePush(Queue* pq, QDataType x);
void QueuePop(Queue* pq);
QDataType QueueFront(Queue* pq);
QDataType QueueBack(Queue* pq);
int QueueSize(Queue* pq);
bool QueueEmpty(Queue* pq);