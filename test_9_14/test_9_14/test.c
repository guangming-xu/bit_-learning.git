#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<assert.h>

//int my_strlen(const char* str)
//{
//	int count = 0;
//	while (*str++)
//	{
//		count++;
//	}
//	return count;
//}
//
//int main()
//{
//	char* s = "abced";
//	printf("%d", my_strlen(s));
//	return 0;
//}


//char* my_strcat(char* dest,char* src)
//{
//	assert(dest && src);
//
//	char* ret = dest;
//	int i = 0;
//	while (*dest)
//	{
//		dest++;
//	}
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = "abc";
//	char arr2[] = { 'd','e','f','\0' };
//
//	printf("%s", my_strcat(arr1, arr2));
//	return 0;
//}


//int my_strcmp(char *str1,char * str2)
//{
//	assert(str1 && str2);
//	while (*str1 ==  *str2) 
//	{
//		if (*str1 == '\0')
//			return 0;
//		str1++;
//		str2++;
//	}
//	return *str1 - *str2;
//}
//
//
//int main()
//{
//	//strcmp 比较的是内容，不是长度
//	char arr1[] = "abcdef";
//	char arr2[] = "abq";
//	int ret = my_strcmp(arr1, arr2);
//	printf("%d", ret);
//
//
//	return 0;
//}

//void my_strncpy(char* s1, char *s2,int sz)
//{
//	assert(s1 && s2);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		if (*(s1 + i - 1 ) == '\0')
//		{
//			*(s1 + i) = '\0';
//		}
//		else
//		{
//			*(s1 + i) = *(s2 + i);
//		}
//	}
//}
//
//
//int main()
//{
//	char arr1[20] = "abcdefghi";
//	char arr2[] = "xxxx";
//	my_strncpy(arr1, arr2, 6);
//
//	printf("%s\n", arr1);
//	return 0;
//}

//void my_strncat(char* s1, char* s2, int sz)
//{
//	assert(s1 && s2);
//	int i = 0;
//	while (*(s1 + i))
//		i++;
//	int j = 0;
//	while (j < sz && *(s2 + j) != '\0')
//	{
//		*(s1 + i++) = *(s2 + j++);
//	}
//	*(s1 + i) = '\0';
//}
//
//int main()
//{
//	char arr1[20] = "abc\0xxxxxx";
//	char arr2[] = "def";
//	my_strncat(arr1, arr2, 2);
//
//	printf("%s", arr1);
//	return 0;
//}

//int my_strncmp(char *s1,char * s2,int sz)
//{
//	assert(s1 && s2);
//	int i = 0;
//	while (i < sz && *(s1 + i) == *(s2 + i))
//	{
//		i++;
//	}
//	if( i == sz)
//		return 0;
//	else
//	{
//		return *(s1 + i) - *(s2 + i);
//	}
//}
//
//int main()
//{
//	char arr1[20] = "abcdef";
//	char arr2[] = "abcqw";
//	int ret = my_strncmp(arr1, arr2, 4);
//	printf("%d\n", ret);
//	return 0;
//}


//
//char* my_strstr(const char* str1, const char* str2)
//{
//	assert(str1, str2);
//
//	char* s1;
//	char* s2;
//	char* cp = str1;
//
//	while (*cp)
//	{
//		s1 = cp;
//		s2 = str2;
//
//		while (*s2 != '\0' && * s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//		{
//			return cp;
//		}
//		cp++;
//	}
//	return NULL;
//}
//
//int main()
//{
//	char arr1[] = "i am good student, hehe student";
//	char arr2[] = "studentq";
//
//	char* ret = my_strstr(arr1, arr2);
//	if (ret == NULL)
//	{
//		printf("找不到\n");
//	}
//	else
//	{
//		printf("%s\n", ret);
//	}
//	return 0;
//
//}



//strtok
//int main()
//{
//	char arr1[] = "xgm@qq.com";
//	char arr2[100] = {0};
//
//	char sep[] = "@.";
//	strcpy(arr2, arr1);
//	char* ret = NULL;
//
//	for (ret = strtok(arr2, sep); ret != NULL; ret = strtok(NULL, sep))
//	{
//		printf("%s\n", ret);
//	}
//	return 0;
//}

//void* my_memcpy(void* dest, const void* src, size_t count)
//{
//	void* ret = dest;
//	while (count--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//
//
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[20] = { 0 };
//	//拷贝整型数组
//	my_memcpy(arr2, arr1, 5 * sizeof(int));
//	int i = 0;
//	for (i = 0; i < 20; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}
