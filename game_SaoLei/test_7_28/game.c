#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"

int tag[ROWS][COLS] = { 0 };
int win = 0;
void InitBoard(char board[ROWS][COLS], int rows, int cols, char set)
{
	int i = 0;
	for (i = 0; i < rows; i++)
	{
		int j = 0;
		for(j = 0; j < cols; j++)
		{
			board[i][j] = set;
		}
	}
}

void DisplayBoard(char board[ROWS][COLS], int row, int col)
{
	printf("------------------------------------\n");
	int i = 0;
	for (i = 0; i <= row; i++)
	{
		printf("%d ", i);
	}
	printf("\n");
	for (i = 0; i <= row; i++)
	{
		printf("- ");
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		int j = 0;
		printf("%d|", i);
		for (j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
	printf("------------------------------------\n");
}


void SetMine(char board[ROWS][COLS], int row, int col)
{
	int count = EASY_COUNT;
	while (count)
	{
		//生成随机下标
		int x = rand() % row + 1;
		int y = rand() % col + 1;
		if (board[x][y] != '1')
		{
			board[x][y] = '1';
			count--;
		}
	}
}


int GetMineCount(char MINE[ROWS][COLS], int x, int y)
{
	return MINE[x - 1][y] +
		MINE[x - 1][y - 1] +
		MINE[x - 1][y + 1] +
		MINE[x][y + 1] +
		MINE[x][y - 1] +
		MINE[x + 1][y + 1] +
		MINE[x + 1][y] +
		MINE[x + 1][y - 1] - 8 * '0';
}

void JudgeAround(char MINE[ROW][COLS],char SHOW[ROWS][COLS], int x, int y)
{
	int i = x - 1;
	for (; i <= x + 1; i++)
	{
		int j = y-1;
		for (; j <= y + 1; j++)
		{
			int count = GetMineCount(MINE, i, j);
			if (tag[i][j] == 0) 
			{
				SHOW[i][j] = '0' + count;
				win++;
				tag[i][j] = 1;
			}
		}
	}
}


void FindMine(char MINE[ROWS][COLS], char SHOW[ROWS][ROWS], int row, int col)
{
	
	int x = 0;
	int y = 0;
	while (win < (row*col - EASY_COUNT))
	{
		printf("请输入排查坐标:>");
		scanf("%d%d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= row)
		{
			if (MINE[x][y] == '1')
			{
				printf("很遗憾你被炸死了！\n");
				DisplayBoard(MINE, ROW, COL);
				break;
			}
			else
			{
				int count = GetMineCount(MINE, x, y);
				if (count == 0)
				{
					JudgeAround(MINE,SHOW, x, y);
				}
				else
				{
					if (tag[x][y] == 0)
					{
						SHOW[x][y] = '0' + count;
						win++;
						tag[x][y] = 1;
					}
					
				}
				DisplayBoard(SHOW, ROW, COL);
				
			}
		}
		else 
		{
			printf("输入有误\n");
		}
	}
	if (win == row * col - EASY_COUNT)
	{
		printf("排雷成功！\n");
		DisplayBoard(MINE, ROW, COL);
	}
}