#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

//int main()
//{
//	unsigned long pulArray[] = { 6,7,8,9,10 };
//	unsigned long* pulPtr;
//	pulPtr = pulArray;
//	*(pulPtr + 3) += 3;
//	printf("%d %d", *pulPtr, *(pulPtr + 3));
//	return 0;
//}

//struct stu
//{
//    int num;
//    char name[10];
//    int age;
//};
//
//
//void fun(struct stu* p)
//{
//    printf("% s\n", (*p).name);
//    return;
//}
//
//
//int main()
//{
//    struct stu students[3] = { {9801,"zhang",20},
//                              {9802,"wang",19},
//                              {9803,"zhao",18}
//    };
//    fun(students + 1);
//    return 0;
//}


//int main()
//{
//	int a = 10;
//	int b = 20;
//
//	int* p = &a;
//	int* q = &b;
//
//	printf("%d", p - q);
//	return 0;
//}

//struct S
//{
//	int a;
//	int b;
//};
//int main()
//{
//	struct S a, * p = &a;
//	a.a = 99;
//	printf("%d\n",(*p).a);
//	return 0;
//}

//int main()
//{
//    char s[100] = "";
//    //scanf("%s", s); //输入时遇到空格就会停止 
//    //scanf("%[^'\n']", s); 可以采用这种，或者gets()
//    gets(s);
//    char* p = s;
//    int count = 0;
//    while (*p != '\0')
//    {
//        count++;
//        p++;
//    }
//    int i;
//    for (i = count - 1; i >= 0; i--)
//    {
//        printf("%c", s[i]);
//    }
//    return 0;
//}

//
//int main()
//{
//	int n = 7;
//	int i;
//	for (i = 1; i <= 7; i++)
//	{
//		int j = 1;
//		for (j; j <= 7 - i; j++)
//		{
//			printf(" ");
//		}
//		int k = 1;
//		for (k; k <= 2 * i - 1; k++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//
//	for (i = 6; i >= 1; i--)
//	{
//		int j = 1;
//		for (j; j <= 7 - i; j++)
//		{
//			printf(" ");
//		}
//		int k = 1;
//		for (k; k <= 2 * i - 1; k++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//
//
//	return 0;
//}

//int empty(int num)
//{
//	if (num <= 1)
//		return 0;
//	if (num % 2 == 0)
//		return num / 2 + empty(num / 2);
//	else
//		return num / 2 + empty(num / 2 + 1);
//}
//
//
//int main()
//{
//	int money = 0;
//	scanf("%d", &money);
//	int count = money + empty(money);
//	printf("%d元可以喝%d瓶",money, count);
//	return 0;
//
//}