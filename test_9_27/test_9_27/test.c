#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//
//int main()
//{
//	//打开文件
//	FILE* pf = fopen("data.txt", "r");
//	if (NULL == pf)
//	{
//		perror("fopen");
//		return -1;
//	}
//	//读文件
//	//随机读写
//	/*int ch = fgetc(pf);
//	printf("%c", ch);*/
//	
//	//指定偏移量读取
//	fseek(pf, 2, SEEK_SET);
//	int ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	fseek(pf, -2, SEEK_CUR);
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	//计算文件指针相对于起始位置的偏移量
//	int ret = ftell(pf);
//	printf("%d\n", ret);
//
//	rewind(pf);
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}


//int main()
//{
//	//打开文件
//	FILE* pf = fopen("data.txt", "wb");
//	if (NULL == pf)
//	{
//		perror("fopen");
//		return -1;
//	}
//	int a = 10000;
//	//以二进制形式写文件
//	fwrite(&a, 4, 1, pf);
//
//	
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}


int main()
{
	//打开文件
	FILE* pf = fopen("data.txt", "r");
	if (NULL == pf)
	{
		perror("fopen");
		return -1;
	}
	int ch = 0;
	
	while ((ch = fgetc(pf)) != EOF)
	{
		printf("%c ", ch);
	}
	//关闭文件
	fclose(pf);
	pf = NULL;

	return 0;
}