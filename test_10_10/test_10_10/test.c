#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

int findmiss(int* num, int numsize)
{
	int x = 0;
	for (int i = 0; i <= numsize; i++)
	{
		x = x ^ i;
	}
	for (int i = 0; i < numsize; i++)
	{
		x = x ^ num[i];
	}
	return x;
}

int main()
{
	int num[10] = { 0,1,2,3,5,6,7,8,9,10 };
	int ret = findmiss(num, 10);
	printf("%d", ret);
	return 0;
}