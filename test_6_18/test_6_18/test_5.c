#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int num[10] = { 1,3,5,6,7,9,10,16,19,25 };
	int min = 0;
	int max = 9;
	int mid = (max + min) / 2;
	int number;
	printf("请输入要查找的数：");
	scanf("%d", &number);
	while (number != num[mid] && min <= max) {
		if (number < num[mid])
		{
			max = mid - 1;
			mid = (max + min) / 2;
		}
		else
		{
			min = mid + 1;
			mid = (max + min) / 2;
		}
	}
	if (min > max) {
		printf("未找到");
	}
	else
	{
		printf("%d", min);
	}
	return 0;
}