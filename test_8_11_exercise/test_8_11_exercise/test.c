#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
//
//int compare_int(const void* a, const void* b)
//{
//	//return (*(int*)a - *(int*)b);  //从小到大排序
//	return (*(int*)b - *(int*)a);  //从大到小排序
//}
//
//int compare_char(const void* a, const void* b)
//{
//	return (*(char*)a - *(char*)b);
//}
//
//int compare_double(const void* a, const void* b)
//{
//	return (*(double*)a > *(double*)b) ? 1:-1;  //因为double两数相减的绝对值可能会小于1，在返回为整形时被当作0处理
//}
//
//int compare_string(const void* a, const void* b)
//{
//	return strcmp((char*)a,(char*)b); 
//}
//  
//int main()   
//{
//	int num[] = { 11,22,5,9,46,32 };   //int类型的排序
//	qsort(num, 6, sizeof(int), compare_int);
//	int i = 0;
//	for (i; i < 6; i++)
//	{
//		printf("%d ", num[i]);
//	}
//	printf("\n");
//
//
//	//char类型的排序
//	char str[] = { 'a','c','B','Z','.','s' };
//	qsort(str, 6, sizeof(char), compare_char);
//	int j = 0;
//	for (j; j < 6; j++)
//	{
//		printf("%c ", str[j]);
//	}
//	printf("\n");
//
//
//	//double类型排序
//	double d_num[] = { 1.1, 3.25, 3.24, 7.0, 6, 3.5 };
//	qsort(d_num, 6, sizeof(double), compare_double);
//	int k = 0;
//	for (k; k < 6; k++)
//	{
//		printf("%lf ", d_num[k]);
//	}
//	printf("\n");
//
//
//	//string类型排序
//	char stri[3][4] = { "cac","abc","bac" };
//	qsort(stri, 3, sizeof(str[0]), compare_string);
//	int l = 0;
//	for (; l < 3; l++)
//	{
//		printf("%s ", stri[l]);
//	}
//	printf("\n");
//	return 0;
//}



int main()
{
	int num[4][5] = { {1,3,4,5,7},{8,10,12,13,15},{16,19,20,22,23},{24,26,27,28,30} };

	printf("请输入要查找的数：");
	int a = 0;
	scanf("%d", &a);
	int row = 0;
	for (row; row < 4; row++)
	{
		if (a == num[row][0])
		{
			printf("该数在第%d行第1列", row);
			return 0;
		}
		else if (a == num[row][4])
		{
			printf("该数在第%d行第5列", row+1);
			return 0;
		}
		else if (a > num[row][0] && a < num[row][4])
		{
			int i = 1;
			for (i; i < 4; i++)
			{
				if (a == num[row][i])
				{
					printf("该数在第%d行第%d列", row + 1,i+1);
					return 0;
				}
			}
			printf("未找到");
			return 0;
		}
	}
	printf("未找到");
	return 0;

}


