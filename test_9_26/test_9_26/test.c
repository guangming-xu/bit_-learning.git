#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

////打开文件
////fopen
////
//int main()
//{
//	//打开文件
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return -1;
//	}
//	//读文件
//	//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}


//
//int main()
//{
//	FILE* pf = fopen("data.txt", "w");
//	if (pf == NULL)
//	{
//		perror(fopen);
//		return -1;
//	}
//	//写文件
//	fputc('b', pf);  //写入字符
//	fputc('i', pf);
//	fputc('t', pf);
//
//
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//
//
//	return 0;
//}


//int main()
//{
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror(fopen);
//		return -1;
//	}
//	//读文件
//	//int ch = fgetc(pf);
//	int ch = fgetc(stdin); //标准输入读，即从键盘接收
//	printf("%c", ch);
//
//	ch = fgetc(stdin);
//	printf("%c", ch);
//
//	ch = fgetc(pf);
//	printf("%c", ch);
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}


//int main()
//{
//	FILE* pf = fopen("data.txt", "w"); //打开文件后，之前的内容会被销毁
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return -1;
//	}
//	fputs("hello world\n", pf);
//	fputs("hello bit\n", pf);
//
//	fclose(pf);
//	pf = NULL;
//}
//
//struct S
//{
//	int n;
//	double d;
//};
//
//int main()
//{
//	struct S s = { 100, 3.14 };
//
//	FILE* pf = fopen("data.txt", "w"); 
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return -1;
//	}
//	//写文件
//	fprintf(pf, "%d %lf", s.n, s.d);
//
//	fclose(pf);
//	pf = NULL;
//}

//struct S
//{
//	int n;
//	double d;
//};
//
//int main()
//{
//	struct S s = { 0 };
//
//	FILE* pf = fopen("data.txt", "r"); 
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return -1;
//	}
//	//读文件
//	fscanf(pf, "%d %lf", &(s.n), &(s.d));
//
//	printf("%d %lf", s.n, s.d);
//
//	fclose(pf);
//	pf = NULL;
//}


////二进制形式写
//struct S
//{
//	int n;
//	double d;
//	char name[10];
//};
//
//int main()
//{
//	struct S s = { 100, 3.14, "zhangsan"};
//
//	FILE* pf = fopen("data.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return -1;
//	}
//	//写文件
//	fwrite(&s, sizeof(s), 1, pf);
//
//	fclose(pf);
//	pf = NULL;
//}


//二进制形式读
struct S
{
	int n;
	double d;
	char name[10];
};

int main()
{
	struct S s = { 100, 3.14, "zhangsan" };

	FILE* pf = fopen("data.txt", "rb");
	if (pf == NULL)
	{
		perror("fopen");
		return -1;
	}
	//读文件——二进制方式读
	fread(&s, sizeof(struct S), 1, pf);

	//打印
	printf("%d %lf %s\n", s.n, s.d, s.name);
	
	fclose(pf);
	pf = NULL;
}
