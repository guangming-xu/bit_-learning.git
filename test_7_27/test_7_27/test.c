#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

void Sort(int arr[], int sz)
{

	int i = 0;
	
	for (i = 0; i < sz; i++)
	{
		int j;
		int flag = 1;
		for (j = 0; j < sz - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int temp;
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
				flag = 0;
			}
		}
		if (flag == 1)
			break;
	}
}

void Print(int arr[],int sz)
{
	
	int i = 0;
	while (i < sz)
	{
		printf("%d  ", arr[i++]);
	}
}

int main()
{
	int arr[] = { 1, 4, 2, 3, 7, 5, 6, 9, 0 };
	int sz = sizeof(arr) / sizeof(int);
	Sort(arr,sz);
	Print(arr,sz);

	return 0;
}